import * as React from "react"
import { Dimensions, Text } from "react-native"
import { Header, ListItem, Input, Icon, Overlay } from "react-native-elements"
import { SafeAreaProvider } from "react-native-safe-area-context"

import { useForm, Controller } from "react-hook-form"

import realm from "../database/getRealmApp.js"

const window = Dimensions.get("window")

const DetailsScreen = ({navigation, route}) => {
    const item = realm.objects("Book").find(d => d.title === route.params.paramKey)
    const { control, handleSubmit, formState: { errors } } = useForm()
    const onSubmit = (form) => {
        realm.write(() => {
            if(item.title !== form.title) item.title = form.title
            if(item.author !== form.author) item.author = form.author
            if(item.category !== form.category) item.category = form.category
        })
        setVisible(false)
    }
    const [visible, setVisible] = React.useState(false)
    return (
        <SafeAreaProvider>
            <Header
                backgroundColor="#3E2221"
                barStyle="default"
                centerComponent={{text: item.title, style: { color: "#fff" } }}
                containerStyle={{ width:window.width }}
                leftComponent={{ icon: "bookmark", color: "#fff" }}
                placement="left"
            />
            <ListItem>
                <ListItem.Chevron size={40} iconStyle={{transform: [{ rotate: '180deg'}]}} onPress={() => navigation.navigate("HomeScreen")}/>
                <ListItem.Content>
                    <ListItem.Title style={{fontSize: 20}}>Auteur : {item.author}</ListItem.Title>
                    <ListItem.Subtitle style={{fontSize: 18}}>Catégorie : {item.category}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <Icon
                reverse
                name='create'
                type='ionicon'
                color='#3E2221'
                size={40}
                containerStyle={{position:"absolute", right:15, bottom:115}}
                onPress={() => setVisible(true)}
            />
            <Icon
                reverse
                name='trash'
                type='ionicon'
                color='#3E2221'
                size={40}
                containerStyle={{position:"absolute", right:15, bottom:15}}
                onPress={() => {
                    realm.write(() => {
                        realm.delete(item)
                        navigation.navigate("HomeScreen")
                    })
                }}
            />
            <Overlay
                isVisible={visible}
                overlayStyle={{backgroundColor:"#3E2221", borderRadius:10, margin:3}}
                onBackdropPress={() => setVisible(false)}
            >
                <Text style={{fontWeight:"bold", fontSize:18, textAlign:"center", color:"white", marginBottom:20}}>Ajouter un livre</Text>
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Titre"
                            leftIcon={{ type: 'font-awesome', name: 'book', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                            errorMessage={errors.title && "Le titre est requis."}
                        />
                    )}
                    name="title"
                    rules={{ required: true }}
                    defaultValue={item.title}
                />
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Auteur"
                            leftIcon={{ type: 'font-awesome', name: 'user', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                            errorMessage={errors.author && "L'auteur est requis."}
                        />
                    )}
                    name="author"
                    rules={{ required: true }}
                    defaultValue={item.author}
                />
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Catégorie"
                            leftIcon={{ type: 'ionicon', name: 'apps', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                        />
                    )}
                    name="category"
                    defaultValue={item.category}
                />
                <Icon
                    reverse
                    name='checkmark-circle-outline'
                    type='ionicon'
                    color='#3E2221'
                    size={30}
                    containerStyle={{alignSelf:"center"}}
                    onPress={handleSubmit(onSubmit)}
                />
            </Overlay>
        </SafeAreaProvider>
    )
}

export default DetailsScreen