import * as React from "react"
import { Dimensions, Platform, FlatList, Text } from "react-native"
import { Header, SearchBar, Input, ListItem, Icon, Overlay } from "react-native-elements"
import { SafeAreaProvider } from "react-native-safe-area-context"
import { useIsFocused } from '@react-navigation/native'

import { useForm, Controller } from "react-hook-form"

import 'react-native-get-random-values'
const BSON = require('bson')

import realm from "../database/getRealmApp.js"

const window = Dimensions.get("window")

const HomeScreen = ({navigation}) => {
    const isFocused = useIsFocused()
    const [realme, setRealme] = React.useState(realm)
    React.useEffect(() => {
        setRealme(realm)
    }, [isFocused, realme])
    const { control, handleSubmit, setValue, formState: { errors } } = useForm()
    const onSubmit = (form) => {
        realm.write(() => {
            realm.create("Book", {
                _id: new BSON.ObjectID(),
                author: form.author,
                category: form.category,
                title: form.title
            })
        })
        setValue("author", "")
        setValue("category", "")
        setValue("title", "")
        setVisible(false)
    }
    const [search, setSearch] = React.useState("")
    const [visible, setVisible] = React.useState(false)
    return (
        <SafeAreaProvider>
            <Header
                backgroundColor="#3E2221"
                barStyle="default"
                centerComponent={{text: "Listes des livres", style: { color: "#fff" } }}
                containerStyle={{ width:window.width }}
                leftComponent={{ icon: "book", color: "#fff" }}
                placement="left"
            />
            <SearchBar
                platform={Platform.OS === "ios" ? "ios" : "android"}
                onChangeText={newVal => setSearch(newVal)}
                placeholder="ex. Hunger Games"
                placeholderTextColor="#858585"
                value={search}
                inputContainerStyle={{flexDirection:"row-reverse"}}
            />
            <FlatList
                keyExtractor={item => item._id.toString()}
                data={search.length === 0 ? realme.objects("Book") : realme.objects("Book").filter(d => d.title.toLowerCase().includes(search.toLowerCase()) || d.author.toLowerCase().includes(search.toLowerCase()))}
                renderItem={({ item }) => (
                    <ListItem bottomDivider>
                        <ListItem.Content>
                            <ListItem.Title style={{fontWeight:"bold"}}>{item.title}</ListItem.Title>
                            <ListItem.Subtitle>{item.author}</ListItem.Subtitle>
                        </ListItem.Content>
                        <ListItem.Chevron size={40} onPress={() => navigation.navigate("DetailsScreen", {paramKey:item.title})}/>
                    </ListItem>
                )}
            />
            <Icon
                reverse
                name='add'
                type='ionicon'
                color='#3E2221'
                size={40}
                containerStyle={{position:"absolute", right:15, bottom:15}}
                onPress={() => setVisible(true)}
            />
            <Overlay
                isVisible={visible}
                overlayStyle={{backgroundColor:"#3E2221", borderRadius:10, margin:3}}
                onBackdropPress={() => setVisible(false)}
            >
                <Text style={{fontWeight:"bold", fontSize:18, textAlign:"center", color:"white", marginBottom:20}}>Ajouter un livre</Text>
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Titre"
                            leftIcon={{ type: 'font-awesome', name: 'book', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                            errorMessage={errors.title && "Le titre est requis."}
                        />
                    )}
                    name="title"
                    rules={{ required: true }}
                />
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Auteur"
                            leftIcon={{ type: 'font-awesome', name: 'user', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                            errorMessage={errors.author && "L'auteur est requis."}
                        />
                    )}
                    name="author"
                    rules={{ required: true }}
                />
                <Controller
                    control={control}
                    render={({ field: { onChange, value } }) => (
                        <Input
                            placeholder="Catégorie"
                            leftIcon={{ type: 'ionicon', name: 'apps', color:'white' }}
                            onChangeText={value => onChange(value)}
                            value={value}
                            containerStyle={{width:window.width-70}}
                            inputStyle={{color:"white"}}
                        />
                    )}
                    name="category"
                />
                <Icon
                    reverse
                    name='checkmark-circle-outline'
                    type='ionicon'
                    color='#3E2221'
                    size={30}
                    containerStyle={{alignSelf:"center"}}
                    onPress={handleSubmit(onSubmit)}
                />
            </Overlay>
        </SafeAreaProvider>
    )
}

export default HomeScreen