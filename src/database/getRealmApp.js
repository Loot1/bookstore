import Realm from "realm"
import BookSchema from "../schemas/BookSchema.js"

const realm = new Realm({
    path: "Book",
    schema: [BookSchema],
    schemaVersion: 1
})

export default realm